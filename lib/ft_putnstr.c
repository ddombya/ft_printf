/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ddombya <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:24:41 by ddombya           #+#    #+#             */
/*   Updated: 2018/02/10 14:30:33 by ddombya          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf.h"

void		ft_putnstr(const char *s, int len)
{
	char c;

	if (s)
	{
		while (len--)
		{
			c = *s;
			write(1, &c, 1);
			s++;
		}
	}
	return ;
}
