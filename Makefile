# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ddombya <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/27 15:58:46 by ddombya           #+#    #+#              #
#    Updated: 2018/02/21 12:21:08 by ddombya          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

FLAGS = -Wall -Wextra -Werror

NAME = libftprintf.a

SRC_PATH = ./
LIB_PATH = ./lib
INC_PATH = ./
OBJ_PATH = ./objs
OBJLIB_PATH = ./obj


SRC_NAME =	ft_find_conv.c\
			ft_find_flags.c\
			ft_find_mod.c\
			ft_find_prc.c\
			ft_find_width.c\
			ft_print_bin.c\
			ft_print_char.c\
			ft_print_hexa.c\
			ft_print_int.c\
			ft_print_mod.c\
			ft_print_octal.c\
			ft_print_prc.c\
			ft_print_ptr.c\
			ft_print_str.c\
			ft_print_u_int.c\
			ft_print_wchar.c\
			ft_print_wstr.c\
			ft_printf.c

LIB_NAME = 	ft_atoi.c\
			ft_bzero.c\
			ft_char_to_str.c\
			ft_itoa.c\
			ft_itoa_base.c\
			ft_itoa_bin.c\
			ft_itoa_oct.c\
			ft_memalloc.c\
			ft_memset.c\
			ft_putchar.c\
			ft_putnstr.c\
			ft_putstr.c\
			ft_strcat.c\
			ft_strchr.c\
			ft_strcmp.c\
			ft_strcpy.c\
			ft_strdel.c\
			ft_strdup.c\
			ft_strjoin.c\
			ft_strjoin_free.c\
			ft_strlen.c\
			ft_strncmp.c\
			ft_strnew.c\
			ft_wc_to_str.c

INC_NAME = ft_printf.h

OBJ_NAME = $(SRC_NAME:.c=.o)
OBJLIB_NAME = $(LIB_NAME:.c=.o)

SRC = $(addprefix $(SRC_PATH)/, $(SRC_NAME))
LIB = $(addprefix $(LIB_PATH)/, $(LIB_NAME))
INC = $(addprefix $(INC_PATH)/, $(INC_NAME))
OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))
OBJLIB = $(addprefix $(OBJLIB_PATH)/,$(OBJLIB_NAME))

all: $(NAME)

$(NAME): $(OBJ) $(OBJLIB) $(INC_NAME)
	@ar rc $(NAME) $(OBJ) $(OBJLIB)
	@ranlib $(NAME)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir -p $(OBJ_PATH) 2> /dev/null || true
	@$(CC) -o $@ -c $<

$(OBJLIB_PATH)/%.o: $(LIB_PATH)/%.c
	@mkdir -p $(OBJLIB_PATH) 2> /dev/null || true
	@$(CC) -o $@ -c $<

clean:
	@rm -rf $(OBJ) $(OBJLIB)

fclean: clean
	@rm -rf ./obj $(NAME)
	@rm -rf $(OBJ_PATH)

re: fclean all

.PHONY: fclean clean re
